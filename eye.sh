#!/bin/sh
export SHELL="/bin/bash"
export PATH="/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin"
XDISP=":0"
XUSER=`who | grep "$XDISP " | awk '{ print $1 }'`
export XAUTHORITY="/home/"$XUSER"/.Xauthority"
DISPLAY=$XDISP notify-send --urgency normal "Пора сделать перерыв" "Глазам нужен отдых"
